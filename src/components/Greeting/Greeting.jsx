import React from 'react';
import Card from '../Common/Card/Card';
import styles from './Greeting.module.css';

export default function Greeting() {
  return (
    <Card /*dark*/ className={styles.summary}>
      <h2>Delicious Food, Delivered To You</h2>
      <p>
        Choose your favourite meal from our broad collection of
        available meals and enjoy delicious lunch or dinner at home.
      </p>
      <p>
        All our meals are cooked with high-quality ingredients,
        just-in-time and of course by experiensed chefs.
      </p>
    </Card>
  );
};