import React from 'react';
import HeaderControl from './HeaderControl';
import styles from './Header.module.css';
import mealsImage from '../../assets/meals.jpg';

export default function Header() {
  return (
    <>
      <header className={styles.header}>
        <h1>ReactMeals</h1>
        <HeaderControl />
      </header>
      <div className={styles['main-image']}>
        <img src={mealsImage} alt='Table full of delicious meals' />
      </div>
    </>
  );
};