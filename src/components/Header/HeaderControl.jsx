import React, { useState } from 'react';
import Modal from '../Common/Modal/Modal';
import Cart from '../Cart/Cart/Cart';
import CartButton from '../Cart/CartButton/CartButton';

export default function HeaderControl() {
  const [ isModalVisible, setIsModalVisible ] = useState(false);

  const hideModal = () => setIsModalVisible(false);
  const showModal = () => setIsModalVisible(true);

  const handleOrder = value => console.log(value);

  return (
    <>
      <CartButton onClick={showModal} />
      <Modal visible={isModalVisible} onHide={hideModal}>
        <Cart onClose={hideModal} onOrder={handleOrder} />
      </Modal>
    </>
  );
}