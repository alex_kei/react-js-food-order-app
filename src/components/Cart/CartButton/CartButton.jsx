import React, { useContext, useEffect, useState } from 'react';
import Button from '../../Common/Button/Button';
import CartIcon from '../CartIcon/CartIcon';
import CartContext from '../../../contexts/cart';
import styles from './CartButton.module.css';

function CartItemsAmountBadge() {
  const { totalAmount } = useContext(CartContext);
  
  if(!totalAmount) {
    return null;
  }
  return <span className={styles.badge}>{totalAmount}</span>;
}

export default function CartButton({ onClick }) {
  const { totalAmount } = useContext(CartContext);
  const [ shouldBump, setShouldBump ] = useState(false);

  useEffect(() => {
    if (!totalAmount) {
      return;
    }

    const ANIMATION_DURATION = 300;   
    
    setShouldBump(true);
    const timeoutId = setTimeout(
      () => setShouldBump(false),
      ANIMATION_DURATION,
    );

    return () => clearTimeout(timeoutId);
  }, [totalAmount]);

  const getClassList = () => {
    const classList = [styles['cart-button']];
    if (shouldBump) {
      classList.push(styles['bump-effect']);
    }
    return classList.join(' ');
  };

  return (
    <Button className={getClassList()} onClick={onClick}>
      <span className={styles.icon}><CartIcon /></span>
      <span>Your Cart</span>
      <CartItemsAmountBadge />
    </Button>
  );
}