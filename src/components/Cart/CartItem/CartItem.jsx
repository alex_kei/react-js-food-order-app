import React from 'react';
import styles from './CartItem.module.css';

import displayPrice from '../../../utils/display-price';

export default function CartItem({
  id,
  name,
  price,
  amount,
  onChangeAmount,
}) {
  if (!amount) {
    return null;
  }

  const removeOne = () => onChangeAmount({
    id,
    name,
    price,
    amount: amount - 1
  });
  
  const addOne = () => onChangeAmount({
    id,
    name,
    price,
    amount: amount + 1
  });

  return (
    <div className={styles['cart-item']}>
      <div>
        <h3>{name}</h3>
        <div className={styles.summary}>
          <span className={styles.price}>{displayPrice(price)}</span>
          <span className={styles.amount}>x{amount}</span>
        </div>
      </div>
      <div className={styles.actions}>
        <button onClick={removeOne}>-</button>
        <button onClick={addOne}>+</button>
      </div>
    </div>
  );
};