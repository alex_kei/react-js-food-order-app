import React, { useContext } from 'react';
import Button from '../../Common/Button/Button';
import CartItem from '../CartItem/CartItem';
import CartContext from '../../../contexts/cart';
import styles from './Cart.module.css';

import displayPrice from '../../../utils/display-price';

export default function Cart({ onClose, onOrder }) {
  const { getCartItems, onChangeAmount } = useContext(CartContext);
  const items = getCartItems();

  const totalPrice = items.reduce(
    (sum, { amount, price }) => sum + amount * price,
    0,
  );

  return (
    <div>
      <ul className={styles['cart-items']}>
        {items.map(
          (item, index) =>
            <li key={index}>
              <CartItem
                { ...item }
                onChangeAmount={onChangeAmount}
              />
            </li>
        )}
      </ul>
      <div className={styles.total}>
        <h3>Total Price</h3>
        <span>{displayPrice(totalPrice)}</span>
      </div>
      <div className={styles.control}>
        <Button className={styles['button--alt']} onClick={onClose}>Close</Button>
        <Button onClick={onOrder}>Order</Button>
      </div>
    </div>
  );
};