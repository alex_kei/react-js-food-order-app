import React from 'react';
import Header from '../Header/Header';
import Greeting from '../Greeting/Greeting';
import MealList from '../Meals/MealList/MealList';
import './App.css';

import mealsData from '../../data/meals';

function App() {

  return (
    <div className="App">
      <Header />
      <Greeting />
      <MealList meals={mealsData} />
    </div>
  );
}

export default App;
