import React from 'react';
import styles from './Card.module.css';

export default function Card({ dark, children, className }) {
  const getClassList = () => {
    const classList = [styles.card];
    if (dark) {
      classList.push(styles.dark);
    }
    if (className) {
      classList.push(className);
    }
    return classList.join(' ');
  }
  
  return <div className={getClassList()}>{children}</div>;
};
