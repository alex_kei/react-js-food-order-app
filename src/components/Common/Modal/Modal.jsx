import React from 'react';
import ReactDOM from 'react-dom';
import Card from '../Card/Card';
import styles from './Modal.module.css'

function Backdrop({ onHide }) {
  return (
    <div className={styles['modal-backdrop']} onClick={onHide}></div>
  );
};

function ModalDialog({ children, onHide }) {
  return (
    <>
      <Backdrop onHide={onHide} />
      <Card className={styles['modal-dialog']}>{children}</Card>
    </>
  );
};

export default function Modal({ children, visible, onHide }) {
  if (!visible) {
    return null;
  }

  return ReactDOM.createPortal(
    <ModalDialog onHide={onHide}>{children}</ModalDialog>,
    document.getElementById('modal'),
  );
};