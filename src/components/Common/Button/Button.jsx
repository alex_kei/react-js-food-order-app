import React from 'react';
import styles from './Button.module.css';

export default function Button({
  type,
  onClick,
  children,
  className,
}) {
  return (
    <button
      className={`${styles.button} ${className ? className : ''}`}
      type={type || 'button'}
      onClick={onClick}
    >
      {children}
    </button>
  );
}