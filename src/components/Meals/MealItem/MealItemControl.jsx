import React, { useState } from 'react';
import Button from '../../Common/Button/Button';
import styles from './MealItemControl.module.css';

const DEFAULT_AMOUNT = 1;

export default function MealItemControl({ id, onSubmit }) {
  const inputId = `amount-input-${id}`;
  const [ amount, setAmount ] = useState(DEFAULT_AMOUNT);

  const parseAmount = ({ target }) => {
    const value = Number(target.value);
    if (value < DEFAULT_AMOUNT) {
      return DEFAULT_AMOUNT;
    }
    return value;
  }

  const handleChange = event => setAmount(
    parseAmount(event),
  );

  const handleSubmit = (event) => {
    event.preventDefault();
    onSubmit(amount)
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className={styles.input}>
          <label htmlFor={inputId}>Amount</label>
          <input
            type='number'
            min={DEFAULT_AMOUNT}
            step='1'
            id={inputId}
            value={amount}
            onChange={handleChange}
          />
        </div>
        <div>
          <Button type='submit'>+Add</Button>
        </div>
      </form>
    </div>
  );
}