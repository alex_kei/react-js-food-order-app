import React, { useContext } from 'react';
import MealItemControl from './MealItemControl';
import CartContext from '../../../contexts/cart';
import styles from './MealItem.module.css';

import displayPrice from '../../../utils/display-price';

export default function MealItem({
  id,
  name,
  description,
  price,
}) {
  const { onAddAmount } = useContext(CartContext);

  const handleSumbit = (amount) => {
    onAddAmount({ id, amount, name, price });
  };

  return (
    <div className={styles.meal}>
      <div>
        <h3>{name}</h3>
        <p className={styles.description}>{description}</p>
        <p className={styles.price}>{displayPrice(price)}</p>
      </div>
      <MealItemControl id={id} onSubmit={handleSumbit} />
    </div>
  );
};
