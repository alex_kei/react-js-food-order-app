import React from 'react';
import Card from '../../Common/Card/Card';
import MealItem from '../MealItem/MealItem';
import styles from './MealList.module.css';

export default function MealList({ meals }) {
  return (
    <Card className={styles['meal-list']}>
      <ul>
        {meals.map(
          meal =>
            <li key={meal.id}>
              <MealItem { ...meal } />
            </li>
        )}
      </ul>
    </Card>
  );
};