import React, { useState } from 'react';

const CartContext = React.createContext({
  getCartItems: () => {},
  onAddAmount: ({id, amount, name, price}) => {},
  onChangeAmount: ({id, amount, name, price}) => {},
  totalAmount: 0,
});

export function CartContextProvider({ children }) {
  const [ cartContent, setCartContent ] = useState(new Map());

  const getCartItems = () => Array.from(cartContent).map(
    ([id, item]) => ({ ...item, id})
  );

  const _updateAmount = (storage, {id, amount, name, price}) => {
    const currentCartContent = new Map(storage);
    if (amount <= 0) {
      currentCartContent.delete(id);
    } else {
      currentCartContent.set(id, { amount, name, price });
    }
    return currentCartContent;
  };

  const onChangeAmount = ({ id, amount, name, price }) => {
    return setCartContent(
      previousCartContent => _updateAmount(
        previousCartContent,
        { id, amount, name, price },
      ),
    );
  };

  const onAddAmount = ({ id, amount, name, price }) => {
    return setCartContent(previousCartContent => {
      const previousAmount = previousCartContent.has(id)
        ? previousCartContent.get(id).amount
        : 0;
      return _updateAmount(
        previousCartContent,
        { id, amount: previousAmount + amount, name, price },
      );
    });
  };

  const totalAmount = Array.from(cartContent).reduce(
    (sum, [id, item]) => sum + item.amount,
    0,
  );

  const context = {
    getCartItems,
    onAddAmount,
    onChangeAmount,
    totalAmount,
  };

  return (
    <CartContext.Provider value={context}>
      {children}
    </CartContext.Provider>
  );
};

export default CartContext;