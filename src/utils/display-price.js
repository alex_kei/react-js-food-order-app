export default function displayPrice(value) {
  const price = Number.parseFloat(value).toFixed(2)
  return `$${price}`;
};